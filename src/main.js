'use strict';

import React, {Component} from 'react';
import {render as renderComponent} from 'react-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import './styles/main.scss';
import {
    WrapperComponent
} from './containers';

import store from './redux/create';
import { LogInContainer, HomeContainer } from "./containers/index";

renderComponent(
    <Provider store={store}>
        <Router >
            <WrapperComponent>
                <div className="main-wrapper">
                    <Route exact path='/login' component={LogInContainer}/>
                    <Route exact path='/home' component={HomeContainer}/>
                </div>
            </WrapperComponent>
        </Router>
    </Provider>
    ,
    document.getElementById('app')
);
