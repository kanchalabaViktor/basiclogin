import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {checkIsLogin} from "../redux/modules/auth.reducer"

@connect(
    ({auth}) => ({
        auth,
    }), {
        checkIsLogin
    }
)

class WrapperComponent extends Component {
    componentWillMount() {
        this.props.checkIsLogin();
        if(!this.props.auth.isLogin){
            this.props.history.push('/login')
        }
    }

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

export default withRouter(WrapperComponent);
