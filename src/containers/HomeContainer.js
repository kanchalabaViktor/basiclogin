import React, {Component} from 'react'
import {withRouter} from 'react-router';
import {connect} from "react-redux";

@connect(
    ({auth}) => ({
        auth
    }), {
    }
)

class HomeContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {login} = this.props.auth;
        return (
            <div className="d-flex justify-content-center">
                <p>{login}</p>
            </div>
    )
    }
}

export default withRouter(HomeContainer)