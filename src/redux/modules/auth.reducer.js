import {
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    CHECK_USER_TOKEN,

} from '../../constants/ActionTypes'
import {setAuthHeader} from '../../helpers/apiClient'


const initialState ={
    isLogin: false,
    loggingIn: false,
    loginError: false,
    login: 'Login'
};

export function auth(state= initialState, action={}) {
    switch(action.type){
        case(LOGIN_START):
            return{
                ...state,
                loggingIn:true,
            };
        case(LOGIN_SUCCESS):{
            setAuthHeader(action.result.data.token);
              return{
                  ...state,
                  loggingIn:false,
                  isLogin:true,
                  loginError:false,
                  login: action.payload
                }
            }
        case(LOGIN_FAIL):{
            return{
                ...state,
                loggingIn:false,
                loginError:action.error.response.data.error,
            }
        }
        case(CHECK_USER_TOKEN):{
            return{
                ...state,
                isLogin:true,
            };
        }
        default:{
            return state;
        }
    }
}

export function logIn(login, password){
    return {
        types: [LOGIN_START, LOGIN_SUCCESS, LOGIN_FAIL],
        payload: login,
        promise: (client) => client.post(`api/auth/login`, {login,password})
    };
}

export function checkIsLogin(){
    let token = localStorage.getItem('UserToken');
    if(token){
        setAuthHeader(token);
        return function(dispatch){
            dispatch({type:CHECK_USER_TOKEN});
        }
    }
}